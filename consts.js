const consts = {
    // MQTT_SERVER_URL: 'mqtt://test.mosquitto.org',
    MQTT_SERVER_URL: 'mqtt://3.17.126.186:1883',
    SPECDATAIDX: 4,
    EVENTIDX: 53,
    MACIDX: 1,
    MSG_TYPE: 0,
    MAC: ['EE74E0EE2CE5', 'D5AB788BD725', 'FACB1D82123C', 'EEEDEA979335', 'DB167A3CE2B7'],
    ABSENT: '3',
    PRESENT: '2',
    CALIBRATING: '1',
    INACTIVE: '-1',
    EVENT_MSG: '$GPRP',
    INACTIVE_TIME_IN_MS: 3600000,
    INACTIVE_CHECK_INTERVAL_IN_MS: 1000
};

let stateColor = {
	
};
stateColor[consts.ABSENT] = 'council__chair--disabled';
stateColor[consts.PRESENT] = 'council__chair--active';
stateColor[consts.CALIBRATING] = 'council__chair--calibrating'
stateColor[consts.INACTIVE] = 'council__chair--noData';


module.exports = { consts, stateColor };