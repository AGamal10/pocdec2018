var mqtt = require('mqtt')
const { consts, stateColor } = require('./consts.js');
var client = mqtt.connect(consts.MQTT_SERVER_URL)
// var client = mqtt.connect('mqtt://3.17.126.186:1883')

// console.log(consts.MQTT_SERVER_URL);

let attendeesCnt = 0;
let curState = [consts.INACTIVE, consts.INACTIVE, consts.INACTIVE, consts.INACTIVE, consts.INACTIVE];
let lastRecTime = [0, 0, 0, 0, 0];


//initPositions();

client.on('connect', function () {
	client.subscribe('nakheel', function (err) {
		if (!err) {
			client.publish('nakheel', 'Hello sensor data')	// irrelevant
		}
	})
})

client.on('message', function (topic, message) {
	// message is Buffer
	console.log(message.toString())
	//	document.getElementById('mqttDev').innerHTML = message.toString();

	message = message.toString();
	if (message == 'Hello sensor data') return;
	else {
		//if (message[MSG_TYPE] == '$GPRP') {
		message = message.split(',');
		//		console.log(message);

		const devMac = message[consts.MACIDX];
		const devState = message[consts.SPECDATAIDX][consts.EVENTIDX];

		const devIdx = consts.MAC.indexOf(devMac);
		if (message[consts.MSG_TYPE] == consts.EVENT_MSG && devIdx !== -1) {
			lastRecTime[devIdx] = new Date().getTime();
			console.log('TYP:', message[consts.MSG_TYPE], 'Sensor:', devMac, '#', devIdx, 'Status:', devState);
			processState(devIdx, devState);
		}
		//}
	}
	//client.end()
})

for (let i = 0; i < 5; i++) lastRecTime[i] = new Date().getTime();

function inactiveCheckLoop() {
	for (let i = 0; i < 5; i++) {
		let curTime = new Date().getTime();
		if (curTime - lastRecTime[i] > consts.INACTIVE_TIME_IN_MS) {
			document.getElementById(`s${i + 1}`).classList = "";
			document.getElementById(`s${i + 1}`).classList.add('council__chair');
			document.getElementById(`s${i + 1}`).classList.add(stateColor[consts.INACTIVE]);
			if (curState[i] == consts.PRESENT) attendeesCnt = attendeesCnt - 1;
			document.getElementById(`attendeesCnt`).innerHTML = `عدد الحضور: ${attendeesCnt}`;
			curState[i] = consts.INACTIVE;
		}
	}

}


setInterval(inactiveCheckLoop, consts.INACTIVE_CHECK_INTERVAL_IN_MS);

function processState(devIdx, devState) {
	console.log(devIdx, curState[devIdx], devState);
	if (curState[devIdx] != devState) {

		if (devState == consts.PRESENT) attendeesCnt = attendeesCnt + 1;
		else if (curState[devIdx] == consts.PRESENT) attendeesCnt = attendeesCnt - 1;
		if (attendeesCnt < 0) attendeesCnt = 0;
		if (attendeesCnt > 5) attendeesCnt = 5;
		curState[devIdx] = devState;

		document.getElementById(`s${devIdx + 1}`).classList = "";
		document.getElementById(`s${devIdx + 1}`).classList.add('council__chair');
		document.getElementById(`s${devIdx + 1}`).classList.add(stateColor[devState]);
		document.getElementById(`attendeesCnt`).innerHTML = `عدد الحضور: ${attendeesCnt}`;

	}

}

